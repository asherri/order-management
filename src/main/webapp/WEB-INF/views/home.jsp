<%--
  Created by IntelliJ IDEA.
  User: asheri
  Date: 15/04/2017
  Time: 04:58 م
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<html>
<head>
    <title>Order Management</title>
</head>
<body>
<div>
    <p>Product Action List</p>
    <ul>
        <spring:url value="/products/create" var="actionUrl"/>
        <li><a href="${actionUrl}">Create new Product</a></li>

        <spring:url value="/products/" var="actionUrl"/>
        <li><a href="${actionUrl}">View Products</a></li>
    </ul>
</div>
<div>
    <p>Order Action List</p>
    <ul>
        <spring:url value="/orders/create" var="actionUrl"/>
        <li><a href="${actionUrl}">Create new Order</a></li>

        <spring:url value="/orders/" var="actionUrl"/>
        <li><a href="${actionUrl}">View Orders</a></li>

    </ul>
</div>
<div>
    <p>Customer Action List</p>
    <ul>
        <spring:url value="/customers/create" var="actionUrl"/>
        <li><a href="${actionUrl}">Create new Customer</a></li>

        <spring:url value="/customers/" var="actionUrl"/>
        <li><a href="${actionUrl}">View Customer</a></li>

    </ul>
</div>

<div>
    <p>Order Item Action List</p>
    <ul>
        <spring:url value="orderItems/create" var="actionUrl"/>
        <li><a href="${actionUrl}">Create new Order Item</a></li>

        <spring:url value="orderItems/view" var="actionUrl"/>
        <li><a href="${actionUrl}">View Order Item</a></li>
    </ul>
</div>
</body>
</html>
