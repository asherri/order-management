<%--
  Created by IntelliJ IDEA.
  User: asheri
  Date: 16/04/2017
  Time: 09:12 م
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <title>Orders</title>
</head>
<body>
<div class="container">
    <div class="component">
        <h2>All Products</h2>
        <table>
            <thead>
            <tr>
                <th>Name</th>
                <th>Customer</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${orders}" var="order">
                <spring:url value="/orders/update/${order.id}" var="orderUpdate"/>
                <spring:url value="/orders/delete/${order.id}" var="orderDelete"/>
                <spring:url value="/orders/${order.id}" var="orderView"/>
                <tr>
                    <td class="name">${order.name}</td>
                    <td class="price">${order.customer.name}</td>
                    <td><a href="${orderView}">View</a> | <a href="${orderUpdate}">Update</a> | <a href="${orderDelete}">Delete</a></td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>
</div>
</body>
</html>
