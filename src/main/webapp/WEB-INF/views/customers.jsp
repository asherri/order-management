<%--
  Created by IntelliJ IDEA.
  User: asheri
  Date: 16/04/2017
  Time: 05:36 م
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<html>
<head>
    <title>Title</title>
</head>
<body>
<div class="container">
    <div class="component">
        <h2>All Products</h2>
        <table>
            <thead>
            <tr>
                <th>Name</th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${customers}" var="customer">
                <spring:url value="/customers/update/${customer.id}" var="customerUpdate"/>
                <spring:url value="/customers/delete/${customer.id}" var="customerDelete"/>
                <spring:url value="/customers/${customer.id}" var="customerView"/>
                <tr>
                    <td class="name">${customer.name}</td>

                    <td><a href="${productView}">View</a> | <a href="${productUpdate}">Update</a> | <a href="${productDelete}">Delete</a></td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>
</div>
</body>
</html>
