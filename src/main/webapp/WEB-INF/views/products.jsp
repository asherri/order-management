<%--
  Created by IntelliJ IDEA.
  User: asheri
  Date: 16/04/2017
  Time: 06:44 م
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<html>
<head>
    <spring:url value="/style/component.css" var="maincss"/>
    <link href="${maincss}" rel="stylesheet">
    <title>All Products</title>
</head>
<body>
<div class="container">
    <div class="component">
        <h2>All Products</h2>
        <table>
            <thead>
            <tr>
                <th>Name</th>
                <th>Price</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${products}" var="product">
                <spring:url value="/products/update/${product.id}" var="productUpdate"/>
                <spring:url value="/products/delete/${product.id}" var="productDelete"/>
                <spring:url value="/products/${product.id}" var="productView"/>
                <tr>
                    <td class="name">${product.name}</td>
                    <td class="price">${product.price}</td>
                    <td><a href="${productView}">View</a> | <a href="${productUpdate}">Update</a> | <a href="${productDelete}">Delete</a></td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
</div>
</div>
</body>
</html>
