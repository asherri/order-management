<%--
  Created by IntelliJ IDEA.
  User: asheri
  Date: 17/04/2017
  Time: 12:00 ص
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <title>Order Items</title>
</head>
<body>
<div class="container">
    <div class="component">
        <h2>All Products</h2>
        <table>
            <thead>
            <tr>
                <th>Name</th>
                <th>Status</th>
                <th>Quantity</th>
                <th>Price</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${orderItems}" var="item">
                <spring:url value="/orderItems/update/${item.id}" var="orderUpdate"/>
                <spring:url value="/orderItems/delete/${item.id}" var="orderDelete"/>
                <spring:url value="/orderItems/${item.id}" var="orderView"/>
                <tr>
                    <td class="name">${item.product.name}</td>
                    <td class="price">${item.price}</td>
                    <td class="quantity">${item.quantity}</td>
                    <td class="status">${item.status}</td>
                    <td><a href="${orderView}">View</a> | <a href="${orderUpdate}">Update</a> | <a href="${orderDelete}">Delete</a></td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>
</div>
</body>
</html>
