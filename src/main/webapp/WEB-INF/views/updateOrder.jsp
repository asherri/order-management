<%--
  Created by IntelliJ IDEA.
  User: asheri
  Date: 16/04/2017
  Time: 07:35 م
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<html>
<head>
    <title>Update Product</title>
</head>
<body>
<spring:url value="/orders/save" var="actionUrl"/>
<form:form action="${actionUrl}" modelAttribute="order">
    <form:input path="name"/>
    <form:input path="customer"/>
    <form:input path="id" cssStyle="display: none;"/>
    <input type="submit" value="Submit">
</form:form>
</body>
</html>
