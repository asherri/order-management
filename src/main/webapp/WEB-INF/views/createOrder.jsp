<%--
  Created by IntelliJ IDEA.
  User: asheri
  Date: 15/04/2017
  Time: 06:33 م
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<html>
<head>
    <title>New Order</title>
</head>
<body>
<div>
    <spring:url value="/orders/save" var="actionUrl"/>

    <form:form modelAttribute="order" action="${actionUrl}" method="get">
        <div class="form-group required">
            <label class="col-sm-2 control-label" for="name">Order Name: </label>
            <div class="col-sm-6">
                <form:input path="name" cssClass="form-control" name="name" placeholder="Product Name" id="name" />
            </div>
        </div>

        <div class="form-group required">
            <label class="col-sm-2 control-label" for="customer">Select Users: </label>
            <div class="col-sm-6">
                <form:select path="customerName" items="${customersName}" id="customer"/>
            </div>
        </div>
        <div class="form-group required">
            <label class="col-sm-2 control-label" for="name">Select Products: </label>
            <div class="col-sm-6">

                <form:checkboxes path="orders" items="${productsName}" name="orders"/>
            </div>
            <div>
            </div>
        </div>

        <input type="submit" value="Submit"/>
    </form:form>
</div>
</body>
</html>
