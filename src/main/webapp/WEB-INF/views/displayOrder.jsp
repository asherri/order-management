<%--
  Created by IntelliJ IDEA.
  User: asheri
  Date: 16/04/2017
  Time: 07:25 م
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <title>Show Order</title>
</head>
<body>
<div>
    <h3>order Name: ${order.name}</h3>
    <h3>order Customer: ${order.customer}</h3>
    <h3>order Price: ${order.price}</h3>
    <c:forEach items="${order.orderItems}" var="item">
        <h2>order Status: ${item.status}</h2>
        <h2>order Price: ${item.price}</h2>
        <h2>order Quantity: ${item.quantity}</h2>
    </c:forEach>

</div>
</body>
</html>
