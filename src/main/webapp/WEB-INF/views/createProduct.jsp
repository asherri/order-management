<%--
  Created by IntelliJ IDEA.
  User: asheri
  Date: 15/04/2017
  Time: 06:46 م
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<html>
<head>
    <title>Create New Product</title>
</head>
<body>
<spring:url value="/products/save" var="actionUrl"/>

<form:form action="${actionUrl}" modelAttribute="product">
    <div class="form-group required">
        <label class="col-sm-2 control-label" for="name">Product Name: </label>
        <div class="col-sm-6">
            <form:input path="name" cssClass="form-control" name="name" placeholder="Product Name" id="name" />
        </div>
    </div>
    <div class="form-group required">
        <label class="col-sm-2 control-label" for="price">Price </label>
        <div class="col-sm-6">
            <div class="inner-addon right-addon">
                <form:input path="price" name="price" id="price" cssClass="form-control"/>
            </div>
        </div>
    </div>
<input type="submit" value="Submit"class="btn"/>
</form:form>
</body>
</html>
