<%--
  Created by IntelliJ IDEA.
  User: asheri
  Date: 15/04/2017
  Time: 06:33 م
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<html>
<head>
    <title>New Order</title>
</head>
<body>
<div>
    <spring:url value="/orderItems/save" var="actionUrl"/>

    <form:form modelAttribute="orderItems" action="${actionUrl}" method="get">
        <div class="form-group required">
            <label class="col-sm-2 control-label" for="dropdown">Select Products: </label>
            <div class="col-sm-6">
                <select name="productName">
                <c:forEach items="${productsName}" var="name">
                    <option name="dropdown" id="dropdown" value="${name}"> ${name} </option>
                </c:forEach>
                </select>
            </div>
            <div>
            </div>
        </div>
        <div class="form-group required">
            <label class="col-sm-2 control-label" for="price">Item Price: </label>
            <div class="col-sm-6">
                <form:input path="price" name="price" />
            </div>
            <div>
            </div>
        </div>
        <div class="form-group required">
            <label class="col-sm-2 control-label" for="price">quantity: </label>
            <div class="col-sm-6">
                <form:input path="quantity" name="quanntity" />
            </div>
            <div>
            </div>
        </div>
        <label>Quantity</label>

        <label>Status</label>
        <div class="form-group required">
            <label class="col-sm-2 control-label" for="price">Item Status: </label>
            <div class="col-sm-6">
                <form:input path="status" name="status"/>
            </div>
            <div>
            </div>
        </div>
        <input type="submit" value="Submit">
    </form:form>
</div>
</body>
</html>
