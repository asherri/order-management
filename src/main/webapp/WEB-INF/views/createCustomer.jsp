<%--
  Created by IntelliJ IDEA.
  User: asheri
  Date: 16/04/2017
  Time: 08:29 م
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <title>Create New Customer</title>
</head>
<body>
<spring:url value="/customers/save" var="actionUrl"/>

<form:form action="${actionUrl}" modelAttribute="customer">
    <label class="col-sm-2 control-label" for="name">Customer Name: </label><form:input path="name"/>
    <input type="submit" value="Submit"/>
</form:form>
</body>
</html>
