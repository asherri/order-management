package com.order.management.order;

import org.springframework.data.repository.CrudRepository;

/**
 * Created by asheri on 16/04/2017.
 */
public interface OrderItemRepository extends CrudRepository<OrderItem, Long> {
}
