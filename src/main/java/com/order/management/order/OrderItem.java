package com.order.management.order;

import com.order.management.core.AbstractEntity;
import com.order.management.core.Product;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import java.math.BigDecimal;

/**
 * Created by asheri on 15/04/2017.
 */

@Entity
public class OrderItem extends AbstractEntity {

    private String status;
    private int quantity;


    @Column(nullable = false)
    private BigDecimal price;

    @ManyToOne(cascade = CascadeType.PERSIST)
    private Product product;

    public OrderItem() {

    }
    public OrderItem(Product product) {
        this.product = product;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

}
