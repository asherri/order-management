package com.order.management.order;

import com.order.management.core.AbstractEntity;
import com.order.management.core.Customer;
import org.springframework.util.Assert;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by asheri on 15/04/2017.
 */

@Entity
@Table(name = "orders")
public class Order extends AbstractEntity {


    @Column(nullable = false)
    private String name;

    @Transient
    private String customerName;

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String[] getOrders() {
        return orders;
    }

    public void setOrders(String[] orders) {
        this.orders = orders;
    }

    @Transient
    private String[] orders;


    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "order_id")
    private List<OrderItem> orderItems = new ArrayList<OrderItem>();

    @ManyToOne(cascade = CascadeType.ALL)
    private Customer customer;

    public Order() {}

    public Order(Customer customer, String name) {

        Assert.notNull(customer);
        Assert.hasText(name, "Name must not be null or empty!");

        this.name = name;
        this.customer = customer;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<OrderItem> getOrderItems() {
        return orderItems;
    }

    public void addOrderItems(OrderItem orderItem) {
        this.orderItems.add(orderItem);
    }


    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public BigDecimal getTotal() {
        BigDecimal  total = orderItems
               .stream()
               .map(OrderItem::getPrice)
               .reduce(BigDecimal.ZERO, BigDecimal::add);
        return total;
    }
}
