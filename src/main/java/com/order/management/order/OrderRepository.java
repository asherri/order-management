package com.order.management.order;

import org.springframework.data.repository.CrudRepository;

/**
 * Created by asheri on 16/04/2017.
 */
public interface OrderRepository extends CrudRepository<Order, Long> {
}
