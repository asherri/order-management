package com.order.management.core;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by asheri on 16/04/2017.
 */
public interface CustomerRepository extends CrudRepository<Customer, Long> {

    Customer findById(Long id);

    List<Customer> findByName(String name);

    Customer save(Customer customer);

    void deleteCustomerById(Long id);
}
