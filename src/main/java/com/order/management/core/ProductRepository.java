package com.order.management.core;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by asheri on 16/04/2017.
 */

@Repository
public interface ProductRepository extends CrudRepository<Product, Long> {

    Product findByName(String name);

}
