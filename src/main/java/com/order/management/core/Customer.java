package com.order.management.core;

import org.springframework.util.Assert;

import javax.persistence.Column;
import javax.persistence.Entity;

/**
 * Created by asheri on 15/04/2017.
 */

@Entity
public class Customer extends AbstractEntity {

    @Column(nullable = false)
    private String name;

    public Customer(String name) {

        Assert.hasText(name, "Name must not be null or empty!");

        this.name = name;
    }

    public Customer() {}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
