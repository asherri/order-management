package com.order.management.controllers;

import com.order.management.core.CustomerRepository;
import com.order.management.core.Product;
import com.order.management.core.ProductRepository;
import com.order.management.order.OrderItem;
import com.order.management.order.OrderItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by asheri on 16/04/2017.
 */

@Controller
@RequestMapping("/orderItems")
public class OrderItemsController {

    @Autowired
    OrderItemRepository orderItemRepository;

    @Autowired
    CustomerRepository customerRepository;

    @Autowired
    ProductRepository productRepository;

    @RequestMapping
    public String getAllOrder(Model model) {
        model.addAttribute("orderItems", this.orderItemRepository.findAll());
        return "orderItems";
    }

    @RequestMapping(value = "/create")
    public String createOrderPage(Model model){
        model.addAttribute("orderItems", new OrderItem());

        List<String> productsName =new ArrayList();
        for (Product product : productRepository.findAll())
            productsName.add(product.getName());
        model.addAttribute("productsName", productsName);

        model.addAttribute("productsName", productsName);

        return "createOrderItems";
    }

    @RequestMapping(value = "/save", method = RequestMethod.GET)
    public String saveOrder(Model model, OrderItem orderItem, @RequestParam(value = "productName", required = false) String productName){
        Product product = productRepository.findByName(productName);
        orderItem.setProduct(product);
        orderItemRepository.save(orderItem);
        model.addAttribute("orderItems", orderItemRepository.findAll());
        return "redirect:/orderItems";
    }


    @RequestMapping(value = "/{orderId}")
    public String getOrder(Model model, @PathVariable("orderId") Long orderId) {
        model.addAttribute("orderItem", this.orderItemRepository.findOne(orderId));
        return "displayOrderItem";
    }


    @RequestMapping(value = "/update/{orderId}")
    public String updateOrder(Model model, @PathVariable("orderId") Long orderId) {
        model.addAttribute("orderItem", this.orderItemRepository.findOne(orderId));
        return "updateOrderItems";
    }

    @RequestMapping(value = "/delete/{id}")
    public String deleteOrder(Model model, @PathVariable("id") Long id){
        orderItemRepository.delete(id);
        model.addAttribute("orders", orderItemRepository.findAll());
        return "redirect:/orders";
    }
}
