package com.order.management.controllers;

import com.order.management.core.Product;
import com.order.management.core.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by asheri on 15/04/2017.
 */


@Controller
@RequestMapping(value = "/products")
public class ProductController {

    @Autowired
    ProductRepository productRepository;

    @RequestMapping
    public String getAllProduct(Model model) {
        model.addAttribute("products", this.productRepository.findAll());
        return "products";
    }

    @RequestMapping(value = "/create")
    public String createProductPage(Model model){
        model.addAttribute("product", new Product());
        return "createProduct";
    }

    @RequestMapping(value = "/save")
    public String saveProduct(Model model, Product product){
        productRepository.save(product);
        model.addAttribute("products", productRepository.findAll());
        return "redirect:/products";
    }


    @RequestMapping(value = "/{productId}")
    public String getProduct(Model model, @PathVariable("productId") Long productId) {
        model.addAttribute("product", this.productRepository.findOne(productId));
        return "displayProduct";
    }


    @RequestMapping(value = "/update/{productId}")
    public String updateProduct(Model model, @PathVariable("productId") Long productId) {
        model.addAttribute("product", this.productRepository.findOne(productId));
        return "updateProduct";
    }

    @RequestMapping(value = "/delete/{id}")
    public String deleteProduct(Model model, @PathVariable("id") Long id){
        productRepository.delete(id);
        model.addAttribute("products", productRepository.findAll());
        return "redirect:/products";
    }

}
