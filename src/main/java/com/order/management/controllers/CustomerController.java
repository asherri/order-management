package com.order.management.controllers;

import com.order.management.core.Customer;
import com.order.management.core.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by asheri on 16/04/2017.
 */

@Controller
@RequestMapping(value = "/customers")
public class CustomerController {

    @Autowired
    CustomerRepository customerRepository;

    @RequestMapping
    public String getAllCustomers(Model model){
        model.addAttribute("customers", customerRepository.findAll());
        return "customers";
    }

    @RequestMapping("/create")
    public String createCustomer(Model model){
        model.addAttribute("customer",new Customer());
        return "createCustomer";
    }

    @RequestMapping(value = "/save")
    public String saveCustomer(Model model, Customer customer){
        customerRepository.save(customer);
        model.addAttribute("customers", customerRepository.findAll());
        return "redirect:/customers";
    }


    @RequestMapping(value = "/{id}")
    public String getCustomer(Model model, @PathVariable("id") Long id) {
        model.addAttribute("customer", this.customerRepository.findOne(id));
        return "displayCustomer";
    }


    @RequestMapping(value = "/update/{id}")
    public String updateCustomer(Model model, @PathVariable("id") Long id) {
        model.addAttribute("customer", this.customerRepository.findOne(id));
        return "updateCustomer";
    }

    @RequestMapping(value = "/delete/{id}")
    public String deleteCustomer(Model model, @PathVariable("id") Long id){
        customerRepository.delete(id);
        model.addAttribute("customers", customerRepository.findAll());
        return "redirect:/customers";
    }

}
