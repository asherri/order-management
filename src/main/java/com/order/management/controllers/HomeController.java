package com.order.management.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by asheri on 15/04/2017.
 */
@Controller
public class HomeController {
    @RequestMapping
    public String getHomePage(){
        return "home";
    }
}
