package com.order.management.controllers;

import com.order.management.core.Customer;
import com.order.management.core.CustomerRepository;
import com.order.management.core.Product;
import com.order.management.core.ProductRepository;
import com.order.management.order.Order;
import com.order.management.order.OrderItem;
import com.order.management.order.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by asheri on 15/04/2017.
 */

@Controller
@RequestMapping("/orders")
public class OrderController {
    @Autowired
    OrderRepository orderRepository;

    @Autowired
    CustomerRepository customerRepository;

    @Autowired
    ProductRepository productRepository;

    @RequestMapping
    public String getAllOrder(Model model) {
        model.addAttribute("orders", this.orderRepository.findAll());
        return "orders";
    }

    @RequestMapping(value = "/create")
    public String createOrderPage(Model model){
        model.addAttribute("order", new Order());

        List<String> customersName =new ArrayList();
        for (Customer customer:customerRepository.findAll())
            customersName.add(customer.getName());
        model.addAttribute("customersName",customersName);

        List<String> productsName =new ArrayList();
        for (Product product : productRepository.findAll())
            productsName.add(product.getName());
        model.addAttribute("productsName", productsName);

        return "createOrder";

    }

    @RequestMapping(value = "/save")
    public String saveOrder(Model model, Order order){

        Customer customer = customerRepository.findByName(order.getCustomerName()).get(0);
        order.setCustomer(customer);
        for (String s:order.getOrders()){
            Product product = productRepository.findByName(s);
            OrderItem item = new OrderItem(product);
            item.setPrice(new BigDecimal(20));
            item.setQuantity(2);
            item.setStatus("approved");
            order.addOrderItems(item);
        }
        orderRepository.save(order);
        model.addAttribute("orders", orderRepository.findAll());
        return "redirect:/orders";
    }


    @RequestMapping(value = "/{orderId}")
    public String getOrder(Model model, @PathVariable("orderId") Long orderId) {
        model.addAttribute("order", this.orderRepository.findOne(orderId));
        return "displayOrder";
    }


    @RequestMapping(value = "/update/{orderId}")
    public String updateOrder(Model model, @PathVariable("orderId") Long orderId) {
        model.addAttribute("order", this.orderRepository.findOne(orderId));
        return "updateOrder";
    }

    @RequestMapping(value = "/delete/{id}")
    public String deleteOrder(Model model, @PathVariable("id") Long id){
        orderRepository.delete(id);
        model.addAttribute("orders", orderRepository.findAll());
        return "redirect:/orders";
    }

}
